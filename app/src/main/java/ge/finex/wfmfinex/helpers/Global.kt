package ge.finex.wfmfinex.helpers

import android.annotation.SuppressLint
import android.app.Application

class Global : Application() {

    @SuppressLint("DiscouragedApi")
    fun getResource(context: Application, resourceId: String): String {
        return context.resources.getString(
            context.resources.getIdentifier(
                resourceId,
                "string",
                context.packageName
            )
        )
    }

    @SuppressLint("DiscouragedApi")
    fun getStringByResourceId(application: Application, resourceId: Int): String {
        return application.resources.getString(resourceId)
    }

    @SuppressLint("DiscouragedApi")
    fun getStringResourceByName(aString: String): String? {
        val packageName = packageName
        val resId = resources.getIdentifier(aString, "string", packageName)
        return getString(resId)
    }


}