package ge.finex.wfmfinex.helpers

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import ge.finex.wfmfinex.models.Comment
import ge.finex.wfmfinex.models.Photo
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.models.User

class DbHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    companion object {
        private const val DB_VERSION = 2
        private const val DB_NAME = "finex.db"
        private const val TABLE_USERS = "finex_users_table"
        private const val TABLE_TASKS = "finex_tasks_table"
        private const val TABLE_PHOTOS = "finex_photos_table"
        private const val TABLE_COMMENTS = "finex_comments_table"

        /**
         * Tasks table structure
         **/
        private const val CREATEDBYID = "createdByID"
        private const val CREATEDBYNAME = "createdByName"
        private const val COMMENT = "comment"

        /**
         * Photos table structure
         **/
        private const val PHOTO = "comment"

        /**
         * Tasks table structure
         **/
        private const val TASKID = "taskID"
        private const val ASSIGNMENTID = "assignmentID"
        private const val TITLE = "title"
        private const val CREATEDATE = "createDate"
        private const val ASSIGMENDATE = "assigmenDate"
        private const val ENDDATE = "endDate"
        private const val STARTDATE = "startDate"
        private const val APROXIMATESTARTDATE = "aproximateStartDate"
        private const val APROXIMATESTARTDATESET = "aproximateStartDateSet"
        private const val APROXIMATEENDDATE = "aproximateEndDate"
        private const val APROXIMATEENDDATESET = "aproximateEndDateSet"
        private const val CURRENTSTATUS = "currentStatus"
        private const val CURRENTACTIONTYPE = "currentActionType"
        private const val DESCRIPTION = "description"
        private const val NOTE = "note"
        private const val UNITADDRESSLINE1 = "unitAddressLine1"
        private const val UNITADDRESSLINE2 = "unitAddressLine2"
        private const val UNITADDRESSPOSTCODE = "unitAddressPostCode"
        private const val UNITNAME = "unitName"
        private const val UNITDESCRIPTION = "unitDescription"
        private const val UNITPOINTX = "unitPointX"
        private const val UNITPOINTY = "unitPointY"
        private const val PRIORITYID = "priorityID"
        private const val PRIORITY = "priority"
        private const val TASKTYPE = "taskType"
        private const val ISSTARTED = "isStarted"
        private const val CANREJECT = "canReject"
        private const val ISREAD = "isRead"
        private const val CITY = "city"
        private const val PRIORITYNAME = "priorityName"
        private const val OPRIORITYID = "opriorityID"

        /**
        finex_users_table fields
         **/
        private const val ID = "iD"
        private const val USERNAME = "userName"
        private const val FORENAME = "forename"
        private const val SURNAME = "surname"
        private const val FULLNAME = "fullName"
        private const val GMT = "gMT"
        private const val REGIONID = "regionID"
        private const val CITYID = "cityID"
        private const val ACTIVEDIRECTORYGUID = "activeDirectoryGuid"
        private const val SESSIONID = "sessionID"
        private const val HASLOCATIONREQUEST = "hasLocationRequest"

        /**
         * Create/Drop TABLE_TASKS
         */
        private const val TASKS_TABLE_STRUCTURE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_TASKS + " ("
                + TASKID + " INTEGER, "
                + ASSIGNMENTID + " INTEGER, "
                + TITLE + " TEXT, "
                + CREATEDATE + " TEXT, "
                + ASSIGMENDATE + " TEXT, "
                + ENDDATE + " TEXT, "
                + STARTDATE + " TEXT, "
                + APROXIMATESTARTDATE + " TEXT, "
                + APROXIMATESTARTDATESET + " TEXT, "
                + APROXIMATEENDDATE + " TEXT, "
                + APROXIMATEENDDATESET + " TEXT, "
                + CURRENTSTATUS + " INTERNET, "
                + CURRENTACTIONTYPE + " INTEGER, "
                + DESCRIPTION + " TEXT, "
                + NOTE + " TEXT, "
                + UNITADDRESSLINE1 + " TEXT, "
                + UNITADDRESSLINE2 + " TEXT, "
                + UNITADDRESSPOSTCODE + " TEXT, "
                + UNITNAME + " TEXT, "
                + UNITDESCRIPTION + " TEXT, "
                + UNITPOINTX + " NUMERIC, "
                + UNITPOINTY + " NUMERIC, "
                + PRIORITYID + " INTEGER, "
                + PRIORITY + " INTEGER, "
                + TASKTYPE + " INTEGER, "
                + ISSTARTED + " INTEGER, "
                + CANREJECT + " NUMERIC, "
                + ISREAD + " NUMERIC, "
                + CITY + " TEXT, "
                + PRIORITYNAME + " TEXT, "
                + OPRIORITYID + " INTEGER)"
                )
        private const val DROP_TASKS_TABLE = "DROP TABLE IF EXISTS $TABLE_TASKS"

        private const val USERS_TABLE_STRUCTURE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_USERS + " ("
                + ID + " INTEGER,"
                + USERNAME + " TEXT,"
                + FORENAME + " TEXT,"
                + SURNAME + " TEXT,"
                + FULLNAME + " TEXT,"
                + GMT + " INTEGER,"
                + REGIONID + " INTEGER,"
                + CITYID + " INTEGER,"
                + ACTIVEDIRECTORYGUID + " TEXT,"
                + SESSIONID + " TEXT,"
                + HASLOCATIONREQUEST + " NUMERIC)"
                )
        private const val DROP_USERS_TABLE = "DROP TABLE IF EXISTS $TABLE_USERS"

        private const val COMMENTS_TABLE_STRUCTURE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_COMMENTS + " ("
                + TASKID + " INTEGER,"
                + CREATEDBYID + " INTEGER,"
                + CREATEDBYNAME + " TEXT,"
                + COMMENT + " TEXT,"
                + CREATEDATE + " TEXT)"
                )
        private const val DROP_COMMENTS_TABLE = "DROP TABLE IF EXISTS $TABLE_COMMENTS"

        private const val PHOTOS_TABLE_STRUCTURE = ("CREATE TABLE IF NOT EXISTS " +
                TABLE_PHOTOS + " ("
                + TASKID + " INTEGER,"
                + PHOTO + " TEXT)"
                )
        private const val DROP_PHOTOS_TABLE = "DROP TABLE IF EXISTS $TABLE_PHOTOS"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(USERS_TABLE_STRUCTURE)
        db.execSQL(TASKS_TABLE_STRUCTURE)
        db.execSQL(COMMENTS_TABLE_STRUCTURE)
        db.execSQL(PHOTOS_TABLE_STRUCTURE)
    }

    override fun onUpgrade(db: SQLiteDatabase, i: Int, i1: Int) {
        db.execSQL(DROP_USERS_TABLE)
        db.execSQL(DROP_TASKS_TABLE)
        db.execSQL(DROP_COMMENTS_TABLE)
        db.execSQL(DROP_PHOTOS_TABLE)
        onCreate(db)
    }

    @SuppressLint("Range", "Recycle")
    fun checkTask(taskId: Int): Boolean? {
        val db = this.writableDatabase

        val selectQuery = /* sql= */"SELECT * FROM $TABLE_TASKS WHERE TASKID = $taskId"
        var cursor: Cursor? = null
        return try {
            cursor = db.rawQuery(selectQuery, null)
            cursor.count > 0
        } finally {
            cursor!!.close()
        }
    }


    @SuppressLint("Range", "Recycle")
    fun getPhotos(taskId: Int): Photo? {
        val db = this.writableDatabase

        val selectQuery = /* sql= */"SELECT * FROM $TABLE_PHOTOS WHERE TASKID = $taskId"
        var cursor: Cursor? = null
        var photos: Photo? = null
        val taskId: Int
        val photo: String

        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor.count > 0) {
                cursor.moveToFirst()
                taskId = cursor.getInt(cursor.getColumnIndex("taskId"))
                photo = cursor.getString(cursor.getColumnIndex("photo"))
//                photos = Photo(
//                    taskId = taskId,
//                    photo = photo
//                )
            }
            return photos
        } catch (e: Exception) {
            return Photo()
        } finally {
            cursor!!.close()
        }
    }

    @SuppressLint("Range", "Recycle")
    fun getComments(task: Int): Comment? {
        val db = this.writableDatabase

        val selectQuery = /* sql= */"SELECT * FROM $TABLE_COMMENTS WHERE TASKID = $task"
        var cursor: Cursor? = null
        var comnt: Comment? = null
        val taskId: Int
        val createdByID: Int
        val createdByName: String
        val comment: String
        val createDate: String

        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor.count > 0) {
                cursor.moveToFirst()
                taskId = cursor.getInt(cursor.getColumnIndex("taskId"))
                createdByID = cursor.getInt(cursor.getColumnIndex("createdByID"))
                createdByName = cursor.getString(cursor.getColumnIndex("createdByName"))
                comment = cursor.getString(cursor.getColumnIndex("comment"))
                createDate = cursor.getString(cursor.getColumnIndex("createDate"))
                comnt = Comment(
                    taskId = taskId,
                    createdByID = createdByID,
                    createdByName = createdByName,
                    comment = comment,
                    createDate = createDate
                )
            }
            return comnt
        } catch (e: Exception) {
            return Comment()
        } finally {
            cursor!!.close()
        }
    }

    @SuppressLint("Range", "Recycle")
    fun getSecondUser(): User {
        val db = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_USERS LIMIT 1"
        var cursor: Cursor? = null
        var usr: User? = null
        val iD: Int
        val userName: String
        val forename: String
        val surname: String
        val fullName: String
        val gMT: Int
        val regionID: Int
        val cityID: Int
        val activeDirectoryGuid: String
        val sessionID: String
        val hasLocationRequest: Boolean
        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor.count > 0) {
                cursor.moveToFirst()
                iD = cursor.getInt(cursor.getColumnIndex("iD"))
                userName = cursor.getString(cursor.getColumnIndex("userName"))
                forename = cursor.getString(cursor.getColumnIndex("forename"))
                surname = cursor.getString(cursor.getColumnIndex("surname"))
                fullName = cursor.getString(cursor.getColumnIndex("fullName"))
                gMT = cursor.getInt(cursor.getColumnIndex("gMT"))
                regionID = cursor.getInt(cursor.getColumnIndex("regionID"))
                cityID = cursor.getInt(cursor.getColumnIndex("cityID"))
                activeDirectoryGuid = cursor.getString(cursor.getColumnIndex("activeDirectoryGuid"))
                sessionID = cursor.getString(cursor.getColumnIndex("sessionID"))
                hasLocationRequest = cursor.getInt(cursor.getColumnIndex("hasLocationRequest")) > 0
                usr = User(
                    iD = iD,
                    userName = userName,
                    forename = forename,
                    surname = surname,
                    fullName = fullName,
                    gMT = gMT,
                    regionID = regionID,
                    cityID = cityID,
                    activeDirectoryGuid = activeDirectoryGuid,
                    sessionID = sessionID,
                    hasLocationRequest = hasLocationRequest
                )
            }
            return usr!!
        } catch (e: Exception) {
            return User()
        } finally {
            cursor!!.close()
        }
    }

    @SuppressLint("Range", "Recycle")
    fun checkUserById(userId: Int): User {
        val db = this.writableDatabase

        val selectQuery = "SELECT * FROM $TABLE_USERS WHERE ID =$userId"
        var cursor: Cursor? = null
        var usr: User? = null
        val iD: Int
        val userName: String
        val forename: String
        val surname: String
        val fullName: String
        val gMT: Int
        val regionID: Int
        val cityID: Int
        val activeDirectoryGuid: String
        val sessionID: String
        val hasLocationRequest: Boolean
        try {
            cursor = db.rawQuery(selectQuery, null)
            if (cursor.count > 0) {
                cursor.moveToFirst()
                iD = cursor.getInt(cursor.getColumnIndex("iD"))
                userName = cursor.getString(cursor.getColumnIndex("userName"))
                forename = cursor.getString(cursor.getColumnIndex("forename"))
                surname = cursor.getString(cursor.getColumnIndex("surname"))
                fullName = cursor.getString(cursor.getColumnIndex("fullName"))
                gMT = cursor.getInt(cursor.getColumnIndex("gMT"))
                regionID = cursor.getInt(cursor.getColumnIndex("regionID"))
                cityID = cursor.getInt(cursor.getColumnIndex("cityID"))
                activeDirectoryGuid = cursor.getString(cursor.getColumnIndex("activeDirectoryGuid"))
                sessionID = cursor.getString(cursor.getColumnIndex("sessionID"))
                hasLocationRequest = cursor.getInt(cursor.getColumnIndex("hasLocationRequest")) > 0

                usr = User(
                    iD = iD,
                    userName = userName,
                    forename = forename,
                    surname = surname,
                    fullName = fullName,
                    gMT = gMT,
                    regionID = regionID,
                    cityID = cityID,
                    activeDirectoryGuid = activeDirectoryGuid,
                    sessionID = sessionID,
                    hasLocationRequest = hasLocationRequest
                )
            }
            return usr!!
        } catch (e: Exception) {
            return User()
        } finally {
            cursor!!.close()
        }
    }

    fun insertTask(task: Task): Long {
        val db = this.writableDatabase

        val cv = ContentValues()
        cv.put(TASKID, task.taskID)
        cv.put(ASSIGNMENTID, task.assignmentID)
        cv.put(TITLE, task.title)
        cv.put(CREATEDATE, task.createDate)
        cv.put(ASSIGMENDATE, task.assigmenDate)
        cv.put(ENDDATE, task.endDate)
        cv.put(STARTDATE, task.startDate)
        cv.put(APROXIMATESTARTDATE, task.aproximateStartDate)
        cv.put(APROXIMATESTARTDATESET, task.aproximateStartDateSet)
        cv.put(APROXIMATEENDDATE, task.aproximateEndDate)
        cv.put(APROXIMATEENDDATESET, task.aproximateEndDateSet)
        cv.put(CURRENTSTATUS, task.currentStatus)
        cv.put(CURRENTACTIONTYPE, task.currentActionType)
        cv.put(DESCRIPTION, task.description)
        cv.put(NOTE, task.note)
        cv.put(UNITADDRESSLINE1, task.unitAddressLine1)
        cv.put(UNITADDRESSLINE2, task.unitAddressLine2)
        cv.put(UNITADDRESSPOSTCODE, task.unitAddressPostCode)
        cv.put(UNITNAME, task.unitName)
        cv.put(UNITDESCRIPTION, task.unitDescription)
        cv.put(UNITPOINTX, task.unitPointX)
        cv.put(UNITPOINTY, task.unitPointY)
        cv.put(PRIORITYID, task.priorityID)
        cv.put(PRIORITY, task.priority)
        cv.put(TASKTYPE, task.taskType)
        cv.put(ISSTARTED, task.isStarted)
        cv.put(CANREJECT, task.canReject)
        cv.put(ISREAD, task.isRead)
        cv.put(CITY, task.city)
        cv.put(PRIORITYNAME, task.priorityName)
        cv.put(OPRIORITYID, task.priorityID)

        val success = db.insert(TABLE_TASKS, null, cv)

        db.close()
        return success
    }

    fun insertUser(user: User): Long {
        val db = this.writableDatabase

        val cv = ContentValues()
        cv.put(ID, user.iD)
        cv.put(USERNAME, user.userName)
        cv.put(FORENAME, user.forename)
        cv.put(SURNAME, user.surname)
        cv.put(FULLNAME, user.fullName)
        cv.put(GMT, user.gMT)
        cv.put(REGIONID, user.regionID)
        cv.put(CITYID, user.cityID)
        cv.put(ACTIVEDIRECTORYGUID, user.activeDirectoryGuid)
        cv.put(SESSIONID, user.sessionID)
        cv.put(HASLOCATIONREQUEST, user.hasLocationRequest)

        val success = db.insert(TABLE_USERS, null, cv)
        db.close()
        return success
    }

    fun insertComment(comment: Comment): Long {
        val db = this.writableDatabase

        val cv = ContentValues()
        cv.put(TASKID, comment.taskId)
        cv.put(CREATEDBYID, comment.createdByID)
        cv.put(CREATEDBYNAME, comment.createdByName)
        cv.put(COMMENT, comment.comment)
        cv.put(CREATEDATE, comment.createDate)

        val success = db.insert(TABLE_COMMENTS, null, cv)
        db.close()
        return success
    }

    fun deleteAllTasks() {
        val db = this.writableDatabase

        db.execSQL(/* sql = */ "DELETE FROM $TABLE_TASKS")
        db.close()
    }

    fun deleteUser(iD: Int) {
        val db = this.writableDatabase

        db.execSQL(/* sql = */ "DELETE FROM $TABLE_USERS WHERE ID = $iD")
        db.close()
    }

    fun deleteAllUser() {
        val db = this.writableDatabase

        db.execSQL(/* sql = */ "DELETE FROM $TABLE_USERS")
        db.close()
    }
}