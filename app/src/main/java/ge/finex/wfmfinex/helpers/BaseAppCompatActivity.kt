package ge.finex.wfmfinex.helpers

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

open class BaseAppCompatActivity : AppCompatActivity() {

    fun toastMessage(stringId: Int) {
        Toast.makeText(
            applicationContext,
            resources.getString(stringId),
            Toast.LENGTH_SHORT
        ).show()
    }

}