package ge.finex.wfmfinex.helpers

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.databinding.ActivityTaskDetailsBinding
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.network.services.GPSTrackerService
import ge.finex.wfmfinex.presentation.*

class TaskDetailFunctions(
    var appCompatActivity: AppCompatActivity? = null,
    var activity: ActivityTaskDetailsBinding? = null,
    var task: Task? = null,
    private val rejectButton: TextView? = activity?.taskDetTaskReject,
    private val acceptButton: Button? = activity?.taskDetTaskAccept,
    private val beginDateTime: Button? = activity?.btnBeginDateTime,
    private val blockBeginComplete: LinearLayout? = activity?.blockBeginComplete,
    private val btnBeginDateTime: Button? = activity?.btnBeginDateTime,
    private val btnCompleteDateTime: Button? = activity?.btnCompleteDateTime,
    private val taskDetailStatusValue: TextView? = activity?.taskDetailStatusValue,
    private val taskDetTaskPlaningStartDate: TextView? = activity?.taskDetTaskPlaningStartDate,
    private val timePosStartDate: Int = task!!.startDate!!.indexOf(" "),
    private val timePosEndDate: Int = task!!.endDate!!.indexOf(" "),
    val onlyTimeEndDate: String = task!!.endDate!!.substring(timePosEndDate + 1, 16),
) {

    fun detailStatusValue(cat: Int?): String? {
        return when (cat) {
            7, 9, 10 -> appCompatActivity?.resources?.getString(R.string.TSK_ACCEPTED)
            0 -> appCompatActivity?.resources?.getString(R.string.TSK_NOT_ACCEPTED)
            1 -> appCompatActivity?.resources?.getString(R.string.TSK_IN_PROGRESS)
            2 -> appCompatActivity?.resources?.getString(R.string.TSK_ON_HOLD)
            4, 11, 12 -> appCompatActivity?.resources?.getString(R.string.TSK_DONE)
            else -> appCompatActivity?.resources?.getString(R.string.TSK_NOT_ACCEPTED)
        }
    }

    fun priorityColor(pc: Int?): Int {
        return when (pc) {
            1 -> -0x1eb9bc //Color.RED; // წითელი
            2 -> -0xf68ef //0xFFF19711; // ფორთოხლისფერი
            3 -> -0x90100 //0xFFFFC400; // ყვითელი
            4 -> -0xb95c10 //Color.BLUE; // ლურჯი
            5 -> -0xb95c10 //Color.BLUE; // ლურჯი
            else -> -0x1eb9bc //Color.RED; // წითელი
        }
    }

    fun taskTypeDescription(taskType: Int?): String {
        return when (taskType) {
            0 -> "ERMS"
            1 -> "OPL"
            2 -> "INT"
            else -> ""
        }
    }

    fun actionBarColor(c: Int?): Int {
        return when (c) {
            0 -> Color.RED
            1 -> -0xff97f4
            2 -> -0x3c00
            3 -> -0xff97f4
            4, 11, 12 -> Color.GRAY
            7, 9, 10 -> -0xff97f4
            else -> Color.BLUE
        }
    }

    fun showSiteInfoSheet() {
        val bottomSheetFragment = BottomSheetFragment()
        val bundle = Bundle()
        bundle.putString("unitAddressLine1", task?.unitAddressLine1)
        bundle.putString("unitDescription", task?.unitDescription)
        bundle.putString(
            "coordinates", "${task?.unitPointX.toString()} , ${task?.unitPointY.toString()}"
        )
        bottomSheetFragment.arguments = bundle
        bottomSheetFragment.show(appCompatActivity!!.supportFragmentManager, "siteInfoSheet")
    }

    @SuppressLint("SetTextI18n")
    fun currentActionCondition() {
        when (task?.currentActionType!!) {
            0 -> {
                rejectButton?.text = appCompatActivity?.getString(R.string.textTaskDecline)
                rejectButton?.visibility = View.VISIBLE
                rejectButton?.setTextColor(Color.RED)
                blockBeginComplete?.visibility = View.GONE
                taskDetTaskPlaningStartDate?.text =
                    "${appCompatActivity?.getString(R.string.textTaskStartDate)}: ${task!!.startDate}"
            }
            1, 3 -> {
                onBeginButton()
            }
            4 -> {
                onDone()
            }
            7 -> {
                rejectButton?.visibility = View.GONE
                acceptButton?.visibility = View.GONE
                blockBeginComplete?.visibility = View.VISIBLE
                beginDateTime?.visibility = View.VISIBLE
                beginDateTime?.isEnabled = true
                btnCompleteDateTime?.visibility = View.GONE
                taskDetTaskPlaningStartDate!!.text =
                    "${appCompatActivity?.getString(R.string.textTaskStartDate)}: ${task!!.aproximateStartDate}"
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun onDone() {
        rejectButton?.visibility = View.GONE
        acceptButton?.visibility = View.GONE
        blockBeginComplete?.visibility = View.GONE
        taskDetTaskPlaningStartDate!!.text =
            "${appCompatActivity?.getString(R.string.textTaskStartDate)}: ${task!!.startDate}"
    }

    @SuppressLint("SetTextI18n")
    fun onAccept(){
        rejectButton?.visibility = View.GONE
        acceptButton?.visibility = View.GONE
        btnCompleteDateTime?.visibility = View.GONE
        blockBeginComplete?.visibility = View.VISIBLE

        btnBeginDateTime?.isEnabled = true
        btnBeginDateTime?.setTextColor(
            ContextCompat.getColor(
                appCompatActivity!!,
                R.color.colorPrimary
            )
        )
        btnBeginDateTime?.background =
            ContextCompat.getDrawable(appCompatActivity!!, R.drawable.button_style)
        btnBeginDateTime?.text = appCompatActivity!!.getString(R.string.textTaskProcessStart)
        taskDetTaskPlaningStartDate?.text =
            "${appCompatActivity!!.getString(R.string.textTaskStartDate)}: ${task!!.aproximateStartDate}"
        taskDetailStatusValue?.text = appCompatActivity!!.getString(R.string.TSK_ACCEPTED)
    }

    @SuppressLint("SetTextI18n")
    fun onBeginButton() {
        rejectButton?.visibility = View.GONE
        acceptButton?.visibility = View.GONE
        blockBeginComplete?.visibility = View.VISIBLE
        btnBeginDateTime?.visibility = View.VISIBLE
        btnCompleteDateTime?.visibility = View.VISIBLE
        btnBeginDateTime?.isEnabled = false
        blockBeginComplete?.isEnabled = true
        btnBeginDateTime?.setTextColor(Color.WHITE)
        btnBeginDateTime?.background =
            ContextCompat.getDrawable(
                appCompatActivity!!,
                R.drawable.button_task_accept_yellow
            )
        taskDetailStatusValue?.text =
            appCompatActivity!!.getString(R.string.TSK_IN_PROGRESS)
        val timePos = task?.startDate?.indexOf(" ")
        val onlyTime = task?.startDate?.substring(timePos!! + 1, 16)
        btnBeginDateTime?.text =
            "${appCompatActivity?.getString(R.string.textTaskProcessStarted)}: $onlyTime"
        taskDetTaskPlaningStartDate!!.text =
            "${appCompatActivity?.getString(R.string.textTaskStartDate)}: ${task!!.startDate}"
    }

    fun showDeclineSheet(coordinates: GPSTrackerService) {
        val declineSheetFragment = DeclineFragment()
        val bundle = Bundle()
        val latitude = coordinates.getLatitude().toString()
        val longitude = coordinates.getLongitude().toString()

        val coordinatesList = ArrayList<String>()
        coordinatesList.add(latitude)
        coordinatesList.add(longitude)
        coordinatesList.add(longitude)

        bundle.putInt("taskAssignmentId", task?.assignmentID!!)
        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        bundle.putStringArrayList("coordinates", ArrayList(coordinatesList))

        declineSheetFragment.arguments = bundle
        declineSheetFragment.show(appCompatActivity!!.supportFragmentManager, "declineSheet")
    }

    fun showCommentSheet(coordinates: GPSTrackerService) {
        val commentSheetFragment = CommentSheetFragment()
        val bundle = Bundle()
        val latitude = coordinates.getLatitude().toString()
        val longitude = coordinates.getLongitude().toString()
        bundle.putInt("taskId", task?.taskID!!)

        val coordinatesList = ArrayList<String>()
        coordinatesList.add(latitude)
        coordinatesList.add(longitude)

        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        bundle.putStringArrayList("coordinates", ArrayList(coordinatesList))

        commentSheetFragment.arguments = bundle
        commentSheetFragment.show(appCompatActivity!!.supportFragmentManager, "commentSheet")
    }

    fun showCommentsListSheet() {
        val commentsListSheetFragment = CommentsListSheetFragment()
        val bundle = Bundle()
        bundle.putInt("taskId", task?.taskID!!)

        commentsListSheetFragment.arguments = bundle
        commentsListSheetFragment.show(
            appCompatActivity!!.supportFragmentManager,
            "commentsListSheet"
        )
    }

    fun showCompleteCommentSheet(coordinates: GPSTrackerService) {
        val acceptedCommentSheetFragment = CompleteCommentSheetFragment()
        val bundle = Bundle()
        val latitude = coordinates.getLatitude().toString()
        val longitude = coordinates.getLongitude().toString()
        bundle.putInt("taskAssignmentID", task?.assignmentID!!)
        bundle.putString("planingStartDate", task?.startDate!!)

        val coordinatesList = ArrayList<String>()
        coordinatesList.add(latitude)
        coordinatesList.add(longitude)

        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        bundle.putStringArrayList("coordinates", ArrayList(coordinatesList))

        acceptedCommentSheetFragment.arguments = bundle
        acceptedCommentSheetFragment.show(
            appCompatActivity!!.supportFragmentManager,
            "acceptedCommentSheet"
        )
    }
}