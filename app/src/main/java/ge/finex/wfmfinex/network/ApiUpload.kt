package ge.finex.wfmfinex.network

import com.google.gson.GsonBuilder
import ge.finex.wfmfinex.network.services.ApiServices
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiUpload {
    private val retrofit: Retrofit
        get() {
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(Interceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("content-type", "multipart/form-data")
                    .build()
                chain.proceed(request)
            })

            val gson = GsonBuilder()
                .setLenient()
                .create()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("http://31.146.208.130:8991/api/mobile/")
                .client(httpClient.build())
                .build()
        }

    fun getService(): ApiServices? {
        return retrofit.create(ApiServices::class.java)
    }

}


//private var retrofit: Retrofit? = null
//
//// This is Client
//private fun retrofitUploadClient() {
//    val logging = HttpLoggingInterceptor()
//    logging.setLevel(HttpLoggingInterceptor.Level.BODY)
//    val httpClient = OkHttpClient.Builder()
//    httpClient.connectTimeout(100, TimeUnit.SECONDS)
//    httpClient.readTimeout(100, TimeUnit.SECONDS)
//    httpClient.writeTimeout(100, TimeUnit.SECONDS)
//    httpClient.addInterceptor(logging) // <-- this is the important line!
//    retrofit = Retrofit.Builder()
//        .baseUrl("http://31.146.208.130:8991/api/mobile/")
//        .addConverterFactory(GsonConverterFactory.create())
//        .client(httpClient.build())
//        .build()
//}