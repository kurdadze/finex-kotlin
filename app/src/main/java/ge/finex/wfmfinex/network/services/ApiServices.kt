package ge.finex.wfmfinex.network.services

import ge.finex.wfmfinex.models.Comment
import ge.finex.wfmfinex.models.Files
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.models.User
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface ApiServices {
    @FormUrlEncoded
    @POST("login")
    fun login(
        @Field("UserName") userName: String?,
        @Field("Password") password: String?,
        @Field("TokenID") token_id: String
    ): Call<User>

    @GET("get_tasks")
    fun getAllTasks(
        @Query("userID") userID: Int,
        @Query("sessionID") sessionID: String?,
        @Query("type") type: Int?,
        @Query("key") key: String?
    ): Call<List<Task>>

    @GET("get_comments/{userId}/{sessionId}/{taskId}")
    fun getComments(
        @Path("userId") userId: Int,
        @Path("sessionId") sessionId: String,
        @Path("taskId") taskId: Int
    ): Call<Collection<Comment>>


    @FormUrlEncoded
    @POST("set_taskaction")
    fun taskAction(
        @Field("userID") userID: Int,
        @Field("sessionID") sessionID: String?,
        @Field("taskAssignmentID") assignmentID: Int,
        @Field("comment") comment: String?,
        @Field("value") code: String?,
        @Field("aproximateStartDate") dateStart: String?,
        @Field("aproximateEndDate") dateEnd: String?,
        @Field("Longitude") longitude: Double,
        @Field("Latitude") latitude: Double,
        @Field("Accuracy") accuracy: Float,
        @Field("Neetwork") network: Boolean?,
        @Field("GPS") GPS: Boolean?
    ): Call<String>

    @FormUrlEncoded
    @POST("add_comment")
    fun addComment(
        @Field("userID") userID: Int,
        @Field("sessionID") sessionID: String?,
        @Field("taskID") taskID: String?,
        @Field("comment") comment: String?,
        @Field("Longitude") longitude: Double,
        @Field("Latitude") latitude: Double,
        @Field("Accuracy") accuracy: Int,
        @Field("Neetwork") network: Boolean?,
        @Field("GPS") GPS: Boolean?,
        @Field("offline") NS: Boolean?,
        @Field("actionDate") commentedDate: String?
    ): Call<String>

    @Multipart
    @POST("upload_file")
    fun uploadPhoto(
        @Part FilePath: MultipartBody.Part,
        @Part("SessionID") sessionId: RequestBody,
        @Part("userID") userId: RequestBody,
        @Part("TaskID") taskId: RequestBody,
        @Part("Longitude") longitude: RequestBody,
        @Part("Latitude") latitude: RequestBody,
        @Part("Neetwork") neetwork: RequestBody,
        @Part("GPS") gps: RequestBody,
        @Part("ICD") icd: RequestBody,
        @Part("IID") iid: RequestBody,
        @Part("SS") ss: RequestBody,
        @Part("Accuracy") accuracy: RequestBody
    ): Call<Void>

    @GET("get_filelist1")
    fun getFiles(
        @Query("userID") UserID: Int,
        @Query("sessionID") SessionID: String?,
        @Query("taskID") TaskID: String?
    ): Call<List<Files?>?>?

    @GET("read_notification")
    fun setTaskReaded(
        @Query("userID") userID: Int,
        @Query("sessionID") sessionID: String?,
        @Query("taskID") taskID: String?
    ): Call<Void?>?

}
