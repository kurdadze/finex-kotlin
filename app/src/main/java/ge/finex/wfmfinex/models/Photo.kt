package ge.finex.wfmfinex.models

import com.google.gson.annotations.SerializedName

class Photo {
    @SerializedName("ID")
    var iD: String? = null

    @SerializedName("Name")
    var name: String? = null

    @SerializedName("Path")
    var path: String? = null

    @SerializedName("UploadDate")
    var uploadDate: String? = null

    @SerializedName("Extention")
    var extention: String? = null

    @SerializedName("UserFullName")
    var userFullName: String? = null

    @SerializedName("CreatedBy_ID")
    var createdBy_ID: String? = null

    @SerializedName("TaskID")
    var taskID: String? = null

    @SerializedName("CanDelete")
    var canDelete: Boolean? = null
}