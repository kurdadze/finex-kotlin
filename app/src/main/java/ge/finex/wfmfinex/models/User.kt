package ge.finex.wfmfinex.models

import com.google.gson.annotations.SerializedName

class User (

    @SerializedName("ID")
    var iD: Int = 0,

    @SerializedName("UserName")
    var userName: String? = null,

    @SerializedName("Forename")
    var forename: String? = null,

    @SerializedName("Surname")
    var surname: String? = null,

    @SerializedName("FullName")
    var fullName: String? = null,

    @SerializedName("GMT")
    var gMT: Int = 0,

    @SerializedName("RegionID")
    var regionID: Int = 0,

    @SerializedName("CityID")
    var cityID: Int = 0,

    @SerializedName("ActiveDirectoryGuid")
    var activeDirectoryGuid: String? = null,

    @SerializedName("SessionID")
    var sessionID: String? = null,

    @SerializedName("HasLocationRequest")
    var hasLocationRequest: Boolean = false
)
