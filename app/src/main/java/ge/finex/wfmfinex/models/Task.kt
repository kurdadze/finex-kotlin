package ge.finex.wfmfinex.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Task(
    @SerializedName("TaskID")
    var taskID: Int? = null,

    @SerializedName("AssignmentID")
    var assignmentID: Int? = null,

    @SerializedName("Title")
    var title: String? = null,

    @SerializedName("CreateDate")
    var createDate: String? = null,

    @SerializedName("AssigmenDate")
    var assigmenDate: String? = null,

    @SerializedName("EndDate")
    var endDate: String? = null,

    @SerializedName("StartDate")
    var startDate: String? = null,

    @SerializedName("AproximateStartDate")
    var aproximateStartDate: String? = null,

    @SerializedName("AproximateStartDateSet")
    var aproximateStartDateSet: String? = null,

    @SerializedName("AproximateEndDate")
    var aproximateEndDate: String? = null,

    @SerializedName("AproximateEndDateSet")
    var aproximateEndDateSet: String? = null,

    @SerializedName("CurrentStatus")
    var currentStatus: Int? = null,

    @SerializedName("CurrentActionType")
    var currentActionType: Int? = null,

    @SerializedName("Description")
    var description: String? = null,

    @SerializedName("Note")
    var note: String? = null,

    @SerializedName("UnitAddressLine1")
    var unitAddressLine1: String? = null,

    @SerializedName("UnitAddressLine2")
    var unitAddressLine2: String? = null,

    @SerializedName("UnitAddressPostCode")
    var unitAddressPostCode: String? = null,

    @SerializedName("UnitName")
    var unitName: String? = null,

    @SerializedName("UnitDescription")
    var unitDescription: String? = null,

    @SerializedName("UnitPointX")
    var unitPointX: Double? = null,

    @SerializedName("UnitPointY")
    var unitPointY: Double? = null,

    @SerializedName("Priority_ID")
    var priorityID: Int? = null,

    @SerializedName("Priority")
    var priority: Int? = null,

    @SerializedName("TaskType")
    var taskType: Int? = null,

    @SerializedName("IsStarted")
    var isStarted: Int? = null,

    @SerializedName("CanReject")
    var canReject: Boolean? = null,

    @SerializedName("IsRead")
    var isRead: Boolean? = null,

    @SerializedName("City")
    var city: String? = null,

    @SerializedName("PriorityName")
    var priorityName: String? = null,

    @SerializedName("OpriorityID")
    var opriorityID: Int? = null

): Serializable