package ge.finex.wfmfinex.models

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("ID")
    var iD: String? = null,
    @SerializedName("TaskID")
    var taskId: Int? = null,
    @SerializedName("CreatedByID")
    var createdByID: Int? = null,
    @SerializedName("CreatedByName")
    var createdByName: String? = null,
    @SerializedName("Comment")
    var comment: String? = null,
    @SerializedName("CreateDate")
    var createDate: String? = null
)