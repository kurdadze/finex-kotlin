package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.helpers.Session
import ge.finex.wfmfinex.helpers.SysHelper
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URLEncoder

class DeclineFragment : BottomSheetDialogFragment() {

    private lateinit var dbHelper: DbHelper
    private lateinit var sysHelper: SysHelper

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dbHelper = DbHelper(requireActivity())
        sysHelper = SysHelper(requireActivity())

        val context = requireContext()
        val view = inflater.inflate(R.layout.decline_fragment, container, false)
        val btnSend = view.findViewById<Button>(R.id.btnSend)
        val commentText = view.findViewById<EditText>(R.id.comment)
        val user = dbHelper.getSecondUser()
        val taskAssignmentId = arguments?.getInt("taskAssignmentId")

        /**
         * latitude
         * longitude
         */
        val coordinates = arguments?.getStringArrayList("coordinates")

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)

        btnSend.setOnClickListener {
            if (commentText.text.length < 5) {
                Toast.makeText(
                    context,
                    getString(R.string.pleaseEnterMinimumFiveSymbol),
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val dateStart = URLEncoder.encode("01.01.2050 12:59", "UTF-8").replace("+", "%20")
                val dateEnd = URLEncoder.encode("01.01.2050 12:59", "UTF-8").replace("+", "%20")
                val latitude = coordinates?.get(0)?.toDouble()
                val longitude = coordinates?.get(1)?.toDouble()
                val comment = commentText.text.toString()
                /**
                 *
                 * კომენტარის დამატება
                 *
                 */
                if (Network.isNetworkAvailable(context = context)) {
                    val commentCall = ApiClient.getService()?.taskAction(
                        userID = user.iD,
                        sessionID = user.sessionID,
                        assignmentID = taskAssignmentId!!,
                        comment = comment,
                        "6",
                        dateStart = dateStart,
                        dateEnd = dateEnd,
                        longitude = longitude!!,
                        latitude = latitude!!,
                        accuracy = Session.currLocationAccuracy,
                        network = Session.currNetwork,
                        GPS = Session.currGPS
                    )
                    commentCall?.enqueue(object : Callback<String> {
                        override fun onResponse(call: Call<String>, response: Response<String>) {
                            if (response.isSuccessful) {
                                dismiss()
                                requireActivity().finish()
                                Toast.makeText(
                                    requireActivity(),
                                    getString(R.string.taskRejected),
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                Toast.makeText(
                                    context,
                                    getString(R.string.failed_to_load_data),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<String>, t: Throwable) {
                            dismiss()
                        }
                    })
                } else {
                    Toast.makeText(
                        context,
                        getString(R.string.internet_connection_is_not_available),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        return view
    }

}
