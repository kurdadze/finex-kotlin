package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.ActionBar
import androidx.camera.video.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.databinding.ActivityTaskDetailsBinding
import ge.finex.wfmfinex.helpers.*
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.models.User
import ge.finex.wfmfinex.network.ApiClient
import ge.finex.wfmfinex.network.services.GPSTrackerService
import me.leolin.shortcutbadger.ShortcutBadger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URLEncoder

class TaskDetailsActivity : BaseAppCompatActivity() {

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_LOCATION = 100
    }

    private lateinit var viewBinding: ActivityTaskDetailsBinding
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var dbHelper: DbHelper
    private lateinit var sysHelper: SysHelper

    private lateinit var tLatitude: String
    private lateinit var tLongitude: String
    private lateinit var user: User
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var googleMap: GoogleMap
    private lateinit var coordinates: GPSTrackerService
    private lateinit var context: Context
    private var actionBar: ActionBar? = null

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityTaskDetailsBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        context = applicationContext
        sysHelper = SysHelper(this)
        dbHelper = DbHelper(this)

//        locationPermissionStatus()

        val task = intent.extras!!.get("currentTask") as Task
        val user = dbHelper.getSecondUser()

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        mapFragment =
            this.supportFragmentManager.findFragmentById(R.id.mapFrame) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback {
            googleMap = it
            val location = LatLng(task.unitPointX!!.toDouble(), task.unitPointY!!.toDouble())
            googleMap.addMarker(MarkerOptions().position(location).title(""))
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12f))
        })

        val taskDetailFunctions = TaskDetailFunctions(
            appCompatActivity = this,
            activity = viewBinding,
            task = task
        )

        val actionColor: Int = taskDetailFunctions.actionBarColor(task.currentActionType)
        val priorityColor: Int = taskDetailFunctions.priorityColor(task.priority)
        val taskTypeDesc = taskDetailFunctions.taskTypeDescription(task.taskType)

        /**
         * ტასკის წაკითხვის სტატუსის განსაზღვრა
         */
        if (!task.isRead!!) {
            val taskReadStatus =ApiClient.getService()?.setTaskReaded(
                userID = user.iD,
                sessionID = user.sessionID,
                taskID = task.taskID.toString()
            )
            taskReadStatus?.enqueue(object : Callback<Void?>{
                override fun onResponse(call: Call<Void?>, response: Response<Void?>) {
//                    TODO("Not yet implemented")
                }

                override fun onFailure(call: Call<Void?>, t: Throwable) {
//                    TODO("Not yet implemented")
                }

            })
        }


        /** ActionBar-ის ტექსტი და ფერი */
        actionBar = supportActionBar
        actionBar!!.setHomeButtonEnabled(true)
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.title = task.unitName
        actionBar!!.setBackgroundDrawable(ColorDrawable(actionColor))

        viewBinding.taskDetPriority.setBackgroundColor(priorityColor)
        viewBinding.taskDetTaskType.text = "$taskTypeDesc ${task.taskID.toString()}"

        coordinates = GPSTrackerService(applicationContext)

        if (task.description == null) {
            viewBinding.taskDetTaskDesc.visibility = View.GONE
        } else {
            viewBinding.taskDetTaskDesc.text =
                getString(R.string.textTaskDescription) + ": " + task.description
        }

        /**
         * ტასკის ობიექტების მდგომარეობის განსაზღვრა
         **/
        if (task.note != null) {
            viewBinding.taskDetTaskNote.visibility = View.VISIBLE
            viewBinding.taskDetTaskNote.text =
                getString(R.string.taskTaskAdditionalInfo) + ": " + task.note
        } else
            viewBinding.taskDetTaskNote.visibility = View.GONE

        /**
         * ტასკის ობიექტების მდგომარეობის განსაზღვრა
         **/
        taskDetailFunctions.currentActionCondition()

        /** Actions */
        /**
         * ტასკის დარეჯექტება
         **/
        viewBinding.taskDetTaskReject.setOnClickListener {
//            resultLauncher.launch(Intent(this, AnotherActivity::class.java))
            coordinates = GPSTrackerService(applicationContext)
            taskDetailFunctions.showDeclineSheet(coordinates)
        }

        /**
         * ტასკის სტატუსის განსაზღვრა
         **/
        viewBinding.taskDetailStatusValue.text =
            getString(R.string.textTaskStatus) + ": " + taskDetailFunctions.detailStatusValue(task.currentActionType)

        /**
         * Sait Info-ს შიტის გახსნა და მონაცემების ჩვენება
         */
        viewBinding.siteInfo.setOnClickListener {
            taskDetailFunctions.showSiteInfoSheet()
        }

        /**
         * ტასკზე კომენტარის შიტის გახსნა
         */
        viewBinding.menuBtnComment.setOnClickListener {
//            val coordinates = getCurrentLocation()
            coordinates = GPSTrackerService(applicationContext)
            taskDetailFunctions.showCommentSheet(coordinates)
            viewBinding.fMenu.close(true)
        }

        viewBinding.taskDetFileList.setOnClickListener {
            val intent = Intent(applicationContext, FileListActivity::class.java)
            intent.putExtra("currentTask", task as java.io.Serializable)
            startActivity(intent)
        }

        /**
         * ტასკზე ფოტოს მიბმა
         */
        viewBinding.menuBtnPhoto.setOnClickListener {
            val intent = Intent(this, CameraActivity::class.java)
            intent.putExtra("taskId", task.taskID)
            startActivity(intent)
            viewBinding.fMenu.close(true)
        }

        /**
         * ტასკზე კომენტარიების სიის შიტის გახსნა
         */
        viewBinding.taskDetComments.setOnClickListener {
            taskDetailFunctions.showCommentsListSheet()
        }

        /**
         * ტასკის მიღების ღილაკზე დაჭერა
         */
        viewBinding.taskDetTaskAccept.setOnClickListener {
            val startDate = URLEncoder.encode(task.startDate?.replace("/", "."),"UTF-8").replace("+", "%20")
            val endDate = URLEncoder.encode(task.endDate?.replace("/", "."),"UTF-8").replace("+", "%20")

            coordinates = GPSTrackerService(applicationContext)

            val latitude = coordinates.getLatitude()
            val longitude = coordinates.getLongitude()

            if (Network.isNetworkAvailable(context = context)) {
                val startTaskCall = ApiClient.getService()?.taskAction(
                    userID = user.iD,
                    sessionID = user.sessionID,
                    assignmentID = task.assignmentID!!,
                    comment = "Accept",
                    "7",
                    dateStart = startDate,
                    dateEnd = endDate,
                    longitude = longitude,
                    latitude = latitude,
                    accuracy = Session.currLocationAccuracy,
                    network = Session.currNetwork,
                    GPS = Session.currGPS
                )
                startTaskCall?.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        taskDetailFunctions.onAccept()
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        toastMessage(R.string.technical_problem)
                    }
                })
            } else {
                toastMessage(R.string.internet_connection_is_not_available)
            }
        }

        /**
         * ტასკის დაწყების ღილაკზე დაჭერა
         */
        viewBinding.btnBeginDateTime.setOnClickListener {

            val dateFirst = URLEncoder.encode(
                sysHelper.getCurrentDateTime("dd/MM/yyyy'T'HH:mm")?.replace("/", "."), "UTF-8"
            ).replace("+", "%20")

            val lines: Array<String> = task.endDate?.split("\n")!!.toTypedArray()

            val dateSecond =
                URLEncoder.encode(lines[0].replace("/", "."), "UTF-8").replace("+", "%20")

            coordinates = GPSTrackerService(applicationContext)

            val latitude = coordinates.getLatitude()
            val longitude = coordinates.getLongitude()

            if (Network.isNetworkAvailable(context = context)) {
                val startTaskCall = ApiClient.getService()?.taskAction(
                    userID = user.iD,
                    sessionID = user.sessionID,
                    assignmentID = task.assignmentID!!,
                    comment = "comm",
                    "1",
                    dateStart = dateFirst,
                    dateEnd = dateSecond,
                    longitude = longitude,
                    latitude = latitude,
                    accuracy = Session.currLocationAccuracy,
                    network = Session.currNetwork,
                    GPS = Session.currGPS
                )
                startTaskCall?.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        taskDetailFunctions.onBeginButton()
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        toastMessage(R.string.technical_problem)
                    }
                })
            } else {
                toastMessage(R.string.internet_connection_is_not_available)
            }
        }

        /**
         * დასრულების ღილაკზე დაჭერა
         */
        viewBinding.btnCompleteDateTime.setOnClickListener {
            coordinates = GPSTrackerService(applicationContext)
            taskDetailFunctions.showCompleteCommentSheet(coordinates)
        }
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
            }
        }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val intent = Intent()
        when (item.itemId) {
            android.R.id.home -> {
                setResult(RESULT_OK, intent)
                onBackPressed()
            }
        }
        return true
    }

    override fun onBackPressed() {
        val intent = Intent()
        setResult(RESULT_OK, intent)
        super.onBackPressed()
    }

}