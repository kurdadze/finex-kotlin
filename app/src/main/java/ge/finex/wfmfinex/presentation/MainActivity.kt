package ge.finex.wfmfinex.presentation

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.adapters.TaskAdapter
import ge.finex.wfmfinex.databinding.ActivityMainBinding
import ge.finex.wfmfinex.helpers.BaseAppCompatActivity
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.models.User
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseAppCompatActivity() {
    private lateinit var viewBinding: ActivityMainBinding
    private lateinit var taskAdapter: TaskAdapter
    private lateinit var task: ArrayList<Task>
    private lateinit var user: User
    private lateinit var context: Context
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var dbHelper: DbHelper
    private val locationPermissionCode = 2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)
        context = this

        dbHelper = DbHelper(context = context)
        val dbHelper = DbHelper(context = applicationContext)

        user = dbHelper.getSecondUser()

        if ((ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(ACCESS_FINE_LOCATION),
                locationPermissionCode
            )
        }

        toggle =
            ActionBarDrawerToggle(this, viewBinding.driverLayout, R.string.open, R.string.close)
        viewBinding.driverLayout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val headerView: View = viewBinding.navView.getHeaderView(0)
        val navUserName = headerView.findViewById<TextView>(R.id.username)
        navUserName.text = user.fullName

        viewBinding.navView.setNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.action_logout -> {
                    dbHelper.deleteAllUser()
                    dbHelper.deleteAllTasks()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()

                }
            }

            true
        }

        viewBinding.container.setOnRefreshListener {
            getTasks()
        }

        viewBinding.container.post {
            getTasks()
        }
    }

    override fun onRestart() {
        super.onRestart()
        getTasks()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == locationPermissionCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "უფლების გაცემა დამოწმებულია", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "უფლების გაცემა უარყოფილია", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (toggle.onOptionsItemSelected(item)) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu, menu)

        val menuItem: MenuItem = menu!!.findItem(R.id.action_search)

        val searchView: androidx.appcompat.widget.SearchView =
            menuItem.actionView as androidx.appcompat.widget.SearchView
        searchView.queryHint = "დაიწყეთ ძებნა..."

        searchView.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener,
            SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                getTasks(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })

        return super.onCreateOptionsMenu(menu)
    }

    private fun getTasks(searchKey: String = "") {
//        val badgeCount = 1
//        ShortcutBadger.applyCount(context, badgeCount) //for 1.1.4+
//        ShortcutBadger.with(applicationContext).count(badgeCount) //for 1.1.3

        if (Network.isNetworkAvailable(context = applicationContext)) {
            viewBinding.container.isRefreshing = true
            val dbHelper = DbHelper(context = applicationContext)
            val userData = dbHelper.getSecondUser()
            val getAllTasksCall = ApiClient.getService()?.getAllTasks(
                userID = userData.iD,
                sessionID = userData.sessionID,
                type = -1,
                key = searchKey
            )
            getAllTasksCall?.enqueue(object : Callback<List<Task>> {
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(call: Call<List<Task>>, response: Response<List<Task>>) {
                    val tasksResponse = response.body()
                    if (tasksResponse != null) {
                        task = ArrayList()
                        for (row in tasksResponse) {
                            task.add(row)
                            if (!dbHelper.checkTask(row.taskID!!)!!) {
                                dbHelper.insertTask(row)
                            }
                        }
                        taskAdapter = TaskAdapter(context = context, tasksList = task)
                        viewBinding.tasksRecycler.layoutManager =
                            LinearLayoutManager(applicationContext)
                        viewBinding.tasksRecycler.adapter = taskAdapter
                        taskAdapter.notifyDataSetChanged()
                    } else {
                        toastMessage(R.string.data_not_found)
                    }
                    viewBinding.container.isRefreshing = false
                }

                override fun onFailure(call: Call<List<Task>>, t: Throwable) {
                    toastMessage(R.string.data_not_loaded)
                    viewBinding.container.isRefreshing = false
                }

            })
        } else {
            toastMessage(R.string.internet_connection_is_not_available)
        }
    }
}