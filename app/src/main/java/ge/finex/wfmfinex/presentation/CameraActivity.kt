package ge.finex.wfmfinex.presentation

import android.Manifest
import android.content.ContentValues
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.video.FallbackStrategy
import androidx.camera.video.Quality
import androidx.camera.video.QualitySelector
import androidx.camera.video.Recorder
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.databinding.ActivityCameraBinding
import ge.finex.wfmfinex.helpers.BaseAppCompatActivity
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.SysHelper
import ge.finex.wfmfinex.models.User
import ge.finex.wfmfinex.network.services.ApiServices
import ge.finex.wfmfinex.network.services.GPSTrackerService
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


@Suppress("UNREACHABLE_CODE")
class CameraActivity : BaseAppCompatActivity() {
    private var TAG = "CameraXApp"
    private lateinit var cameraExecutor: ExecutorService
    private lateinit var viewBinding: ActivityCameraBinding
    private var imageCapture: ImageCapture? = null
    private var taskId: Int? = 0
    private lateinit var user: User
    private lateinit var sysHelper: SysHelper
    private lateinit var dbHelper: DbHelper
    private lateinit var gps: GPSTrackerService

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_CAMERA = 10
        private val REQUIRED_PERMISSIONS =
            mutableListOf(
                Manifest.permission.CAMERA,
            ).apply {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                    add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }
            }.toTypedArray()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityCameraBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        sysHelper = SysHelper(this)
        dbHelper = DbHelper(this)

        user = dbHelper.getSecondUser()

        gps = GPSTrackerService(applicationContext)

        taskId = intent.getIntExtra("taskId", 0)

        cameraPermissionStatus()

        viewBinding.imageCaptureButton.setOnClickListener { takePhoto() }

        cameraExecutor = Executors.newSingleThreadExecutor()
    }

    private fun initCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(viewBinding.viewFinder.surfaceProvider)
                }
            val recorder = Recorder.Builder()
                .setQualitySelector(
                    QualitySelector.from(
                        Quality.HIGHEST,
                        FallbackStrategy.higherQualityOrLowerThan(Quality.SD)
                    )
                )
                .build()

            imageCapture = ImageCapture.Builder().build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera

                // For image and video
                cameraProvider.bindToLifecycle(
                    this,
                    cameraSelector,
                    preview,
                    imageCapture
                )

            } catch (exc: Exception) {
                Log.e(TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(this))

    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return

        val pictureName =
            "${taskId.toString()} - ${sysHelper.getDateTimeNow("yyyy-MM-dd HH_mm_ss")}"

        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, pictureName)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
                put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/Finex")
            }
        }

        val outputOptions = ImageCapture.OutputFileOptions
            .Builder(
                contentResolver,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                contentValues
            )
            .build()

        imageCapture.takePicture(
            outputOptions,
            ContextCompat.getMainExecutor(this),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val bos: ByteArrayOutputStream
                    val bmp: Bitmap
                    val filePathImg =
                        Environment.getExternalStoragePublicDirectory("Pictures/Finex").toString()
                    val imageBase64 = sysHelper.fileToBase64("$filePathImg/$pictureName.jpg")

                    val featuredImage = File("$filePathImg/$pictureName.jpg")
                    if (featuredImage.exists()) {
                        bmp = BitmapFactory.decodeFile(featuredImage.absolutePath)
                        bos = ByteArrayOutputStream()
                        bmp.compress(Bitmap.CompressFormat.JPEG, 30, bos)
                    }

                    val retrofit =
                        Retrofit.Builder().baseUrl("http://31.146.208.130:8991/api/mobile/")
                            .addConverterFactory(GsonConverterFactory.create()).build()

                    val requestFile =
                        featuredImage.asRequestBody("multipart/form-data".toMediaTypeOrNull())

                    val filePath = MultipartBody.Part.createFormData(
                        "FilePath",
                        featuredImage.name,
                        requestFile
                    )
                    val sessionId: RequestBody = user.sessionID.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val userId: RequestBody = user.iD.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val taskId1: RequestBody = taskId.toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val longitude: RequestBody = gps.getLongitude().toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val latitude: RequestBody = gps.getLatitude().toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val neetwork: RequestBody = "false".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val gps1: RequestBody = "true".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val icd: RequestBody = "1900/02/12 12:15:40".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val iid: RequestBody = "-1".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val ss: RequestBody = "online".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                    val accuracy: RequestBody = "1.0".toRequestBody("multipart/form-data".toMediaTypeOrNull())

                    val apiService: ApiServices = retrofit.create(ApiServices::class.java)
                    val call: Call<Void> = apiService.uploadPhoto(
                        filePath,
                        sessionId,
                        userId,
                        taskId1,
                        longitude,
                        latitude,
                        neetwork,
                        gps1,
                        icd,
                        iid,
                        ss,
                        accuracy
                    )

                    call.enqueue(object : Callback<Void?> {
                        override fun onResponse(call: Call<Void?>, response: Response<Void?>) {
                            if (response.isSuccessful) {
                                toastMessage(R.string.photoUploaded)
                                finish()
                            }
                        }

                        override fun onFailure(call: Call<Void?>, t: Throwable) {
                            Toast.makeText(applicationContext, t.toString(), Toast.LENGTH_SHORT)
                                .show()
                        }
                    })

                    /**
                     * უნდა მოხდეს imageBase64-ს მნიშვნელობის ბაზაში ჩაწერა
                     * და შემდეგ სერვერზე გადაგზავნა
                     *
                     * პროცესის დასრულების შემდგომ მოხდეს ფაილის ფიზიკურად წაშლა ფოლდერიდან
                     */
                }
            }
        )
    }

    private fun allPermissionsGranted() = CameraActivity.REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun cameraPermissionStatus() {
        if (allPermissionsGranted()) {
            initCamera()
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, PERMISSION_REQUEST_ACCESS_CAMERA
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_ACCESS_CAMERA) {
            if (allPermissionsGranted()) {
                initCamera()
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT)
                    .show()
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

}