package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.helpers.Session
import ge.finex.wfmfinex.helpers.SysHelper
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URLEncoder

class CompleteCommentSheetFragment : BottomSheetDialogFragment() {

    private lateinit var dbHelper: DbHelper
    private lateinit var sysHelper: SysHelper

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dbHelper = DbHelper(requireActivity())
        sysHelper = SysHelper(requireActivity())

        val context = requireContext()
        val view = inflater.inflate(R.layout.complete_comment_sheet_fragment, container, false)
        val btnCancel = view.findViewById<Button>(R.id.btnCancel)
        val btnSend = view.findViewById<Button>(R.id.btnSend)
        val commentText = view.findViewById<EditText>(R.id.comment)
        val user = dbHelper.getSecondUser()
        val taskAssignmentID = arguments?.getInt("taskAssignmentID")
        val planingStartDate = arguments?.getString("planingStartDate")

        /**
         * latitude
         * longitude
         */
        val coordinates = arguments?.getStringArrayList("coordinates")

        fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(context)

        btnCancel.setOnClickListener {
            dismiss()
        }

        btnSend.setOnClickListener {
            val latitude = coordinates?.get(0)?.toDouble()
            val longitude = coordinates?.get(1)?.toDouble()
            val dateFirst =
                URLEncoder.encode(
                    sysHelper.getCurrentDateTime("dd/MM/yyyy'T'HH:mm")?.replace("/", "."), "UTF-8"
                ).replace("+", "%20")
            val dateSecond =
                URLEncoder.encode(planingStartDate?.replace("/", "."), "UTF-8").replace("+", "%20")

            /**
             *
             * კომენტარის დამატება
             *
             */
            if (Network.isNetworkAvailable(context = context)) {
                val completeCall = ApiClient.getService()?.taskAction(
                    userID = user.iD,
                    sessionID = user.sessionID,
                    assignmentID = taskAssignmentID!!,
                    comment = commentText.text.toString(),
                    "4",
                    dateStart = dateFirst,
                    dateEnd = dateSecond,
                    longitude = longitude!!,
                    latitude = latitude!!,
                    accuracy = Session.currLocationAccuracy,
                    network = Session.currNetwork,
                    GPS = Session.currGPS
                )
                completeCall?.enqueue(object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        dismiss()
                        activity?.finish()
                        Toast.makeText(
                            context,
                            getString(R.string.task_completed),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Toast.makeText(
                            context,
                            getString(R.string.technical_problem),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                })
            } else {
                Toast.makeText(
                    context,
                    getString(R.string.internet_connection_is_not_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        return view
    }

}
