package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.adapters.CommentAdapter
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.helpers.SysHelper
import ge.finex.wfmfinex.models.Comment
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommentsListSheetFragment : BottomSheetDialogFragment() {

    private lateinit var dbHelper: DbHelper
    private lateinit var sysHelper: SysHelper
    private lateinit var comment: ArrayList<Comment>
    private lateinit var commentAdapter: CommentAdapter

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        dbHelper = DbHelper(requireActivity())
        sysHelper = SysHelper(requireActivity())

        val view = inflater.inflate(R.layout.commentslistsheet_fragment, container, false)
        val arrowDown = view.findViewById<ImageView>(R.id.arrow_down)
        val recyclerView = view.findViewById<RecyclerView>(R.id.comments_list_view)
        val user = dbHelper.getSecondUser()
        val taskId = arguments?.getInt("taskId")

        arrowDown.setOnClickListener {
            dismiss()
        }

        getComments(user.iD, user.sessionID!!, taskId!!, recyclerView)

        return view
    }

    private fun getComments(
        userId: Int,
        sessionId: String,
        taskId: Int,
        recyclerView: RecyclerView
    ) {
        if (Network.isNetworkAvailable(context = requireContext())) {
            val getAllComments = ApiClient.getService()?.getComments(
                userId = userId,
                sessionId = sessionId,
                taskId = taskId
            )
            getAllComments?.enqueue(object : Callback<Collection<Comment>> {
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<Collection<Comment>>,
                    response: Response<Collection<Comment>>
                ) {
                    val commentsResponse = response.body()
                    if (commentsResponse != null) {
                        comment = ArrayList()
                        for (row in commentsResponse) {
                            comment.add(row)
                        }
                        commentAdapter = CommentAdapter(commentsList = comment)
                        recyclerView.layoutManager = LinearLayoutManager(requireContext())
                        recyclerView.adapter = commentAdapter
                        commentAdapter.notifyDataSetChanged()

                    } else {
                        Toast.makeText(
                            requireContext(),
                            "მონაცემები არ არსებობს",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<Collection<Comment>>, t: Throwable) {
                    Toast.makeText(
                        requireContext(),
                        "მონაცემები ვერ ჩაიტვირთა!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
        } else {
            Toast.makeText(
                requireContext(),
                getString(R.string.internet_connection_is_not_available),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}
