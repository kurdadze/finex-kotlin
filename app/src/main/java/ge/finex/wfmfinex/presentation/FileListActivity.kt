package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.adapters.CommentAdapter
import ge.finex.wfmfinex.adapters.FileListAdapter
import ge.finex.wfmfinex.databinding.ActivityFileListBinding
import ge.finex.wfmfinex.helpers.BaseAppCompatActivity
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.helpers.SysHelper
import ge.finex.wfmfinex.models.Files
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FileListActivity : BaseAppCompatActivity() {

    private lateinit var dbHelper: DbHelper
    private lateinit var sysHelper: SysHelper
    private lateinit var file: ArrayList<Files>
    private lateinit var fileListAdapter: FileListAdapter

    private lateinit var viewBinding: ActivityFileListBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityFileListBinding.inflate(layoutInflater)
        val fileRecycler = viewBinding.fileRecycler
        setContentView(viewBinding.root)

        dbHelper = DbHelper(applicationContext)
        sysHelper = SysHelper(applicationContext)

        val user = dbHelper.getSecondUser()
        val task = intent.extras!!.get("currentTask") as Task

        getFiles(
            userId = user.iD,
            sessionId = user.sessionID.toString(),
            taskId = task.taskID!!,
            fileRecycler = fileRecycler
        )
    }

    private fun getFiles(
        userId: Int,
        sessionId: String,
        taskId: Int,
        fileRecycler: RecyclerView
    ) {
        if (Network.isNetworkAvailable(context = applicationContext)) {
            val commentCall = ApiClient.getService()?.getFiles(
                userId,
                sessionId,
                taskId.toString()
            )
            commentCall?.enqueue(object : Callback<List<Files?>?> {
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<List<Files?>?>,
                    response: Response<List<Files?>?>
                ) {
                    val fileResponse = response.body()
                    if (fileResponse != null) {
                        file = ArrayList()
                        for (row in fileResponse) {
                            file.add(row!!)
                        }
                        fileListAdapter = FileListAdapter(applicationContext, file)
                        fileRecycler.layoutManager = LinearLayoutManager(applicationContext)
                        fileRecycler.adapter = fileListAdapter
                        fileListAdapter.notifyDataSetChanged()

                    } else {
                        toastMessage(R.string.data_not_found)
                    }
                }

                override fun onFailure(call: Call<List<Files?>?>, t: Throwable) {
                    Toast.makeText(applicationContext, "Failure", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            toastMessage(R.string.internet_connection_is_not_available)
        }
    }
}