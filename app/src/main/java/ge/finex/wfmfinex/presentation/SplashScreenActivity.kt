package ge.finex.wfmfinex.presentation

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import ge.finex.wfmfinex.databinding.ActivitySplashScreenBinding
import ge.finex.wfmfinex.helpers.BaseAppCompatActivity
import ge.finex.wfmfinex.helpers.DbHelper

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : BaseAppCompatActivity() {

    private lateinit var viewBinding: ActivitySplashScreenBinding
    private lateinit var handler: Handler
    private lateinit var dbHelper: DbHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        dbHelper = DbHelper(this)

        handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            val user = dbHelper.getSecondUser()
            val intent: Intent = if (user.iD > 0) {
                Intent(this, MainActivity::class.java)
            } else {
                Intent(this, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()
        }, 2000)

    }
}