package ge.finex.wfmfinex.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ge.finex.wfmfinex.R

class BottomSheetFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.bottomsheet_fragment, container, false)
        val addressPosition = view.findViewById<TextView>(R.id.addressPosition)
        val information = view.findViewById<TextView>(R.id.information)
        val coordinates = view.findViewById<TextView>(R.id.coordinates)

        val addressPositionVal = arguments?.getString("unitAddressLine1")
        val informationVal = arguments?.getString("unitDescription")
        val coordinatesVal = arguments?.getString("coordinates")

        addressPosition.text = addressPositionVal
        information.text = informationVal
        coordinates.text = coordinatesVal

        return view
    }

}