package ge.finex.wfmfinex.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.firebase.messaging.FirebaseMessaging
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.databinding.ActivityLoginBinding
import ge.finex.wfmfinex.helpers.BaseAppCompatActivity
import ge.finex.wfmfinex.helpers.DbHelper
import ge.finex.wfmfinex.helpers.Network
import ge.finex.wfmfinex.models.User
import ge.finex.wfmfinex.network.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseAppCompatActivity() {

    private lateinit var viewBinding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        viewBinding.btnLogin.setOnClickListener {
            val userName = viewBinding.edtUsername.text.toString()
            val password = viewBinding.edtPassword.text.toString()
            val token: String = FirebaseMessaging.getInstance().token.toString()
            logIn(userName = userName, password = password, token = token)
        }
    }

    private fun logIn(userName: String, password: String, token: String) {
        setupProgressBar(true)
        if (Network.isNetworkAvailable(context = applicationContext)) {
            val dbHelper = DbHelper(context = applicationContext)
            val loginCall = ApiClient.getService()
                ?.login(userName = userName, password = password, token_id = token)
            loginCall?.enqueue(object : Callback<User> {
                override fun onResponse(call: Call<User>, response: Response<User>) {
                    val userResponse = response.body()
                    if (userResponse != null) {
                        val user = dbHelper.checkUserById(userResponse.iD)
                        if (user.iD > 0) {
                            dbHelper.deleteUser(user.iD)
                            dbHelper.deleteAllTasks()
                        }
                        dbHelper.insertUser(
                            User(
                                iD = userResponse.iD,
                                userName = userResponse.userName,
                                forename = userResponse.forename,
                                surname = userResponse.surname,
                                fullName = userResponse.forename + " " + userResponse.surname,
                                gMT = userResponse.gMT,
                                regionID = userResponse.regionID,
                                cityID = userResponse.cityID,
                                activeDirectoryGuid = userResponse.activeDirectoryGuid,
                                sessionID = userResponse.sessionID,
                                hasLocationRequest = userResponse.hasLocationRequest
                            )
                        )
                        val intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        setupProgressBar(false)
                        finish()
                    } else {
                        toastMessage(R.string.authorization_failed)
                        setupProgressBar(false)
                    }
                }

                override fun onFailure(call: Call<User>, t: Throwable) {
                    toastMessage(R.string.internet_connection_is_not_available)
                    setupProgressBar(false)
                }
            })
        } else {
            toastMessage(R.string.internet_connection_is_not_available)
            setupProgressBar(false)
        }
    }

    private fun setupProgressBar(state: Boolean) {
        val progressBar = viewBinding.progressBarId
        if (state)
            progressBar.visibility = View.VISIBLE
        else
            progressBar.visibility = View.GONE
    }

}