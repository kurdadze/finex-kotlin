package ge.finex.wfmfinex.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.helpers.Session
import ge.finex.wfmfinex.models.Comment
import ge.finex.wfmfinex.models.Files

class FileListAdapter(private var context: Context, private val fileList: List<Files>) :
    RecyclerView.Adapter<FileListAdapter.FileViewFolder>() {

    inner class FileViewFolder(v: View) : RecyclerView.ViewHolder(v) {
        var imgId = v.findViewById<ImageView>(R.id.imgId)!!
        var deleteId = v.findViewById<TextView>(R.id.deleteId)!!
    }

    override fun onBindViewHolder(holder: FileViewFolder, position: Int) {
        val file = fileList[position]
        holder.deleteId.setOnClickListener {
            Toast.makeText(context, Session.base_url + file.path, Toast.LENGTH_SHORT).show()
        }
//        holder.imgId.setImageBitmap()
        Glide
            .with(context)
            .load<Any>(Session.base_url + file.path)
            .into(holder.imgId)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileViewFolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.file_list_item, parent, false)
        return FileViewFolder(v)
    }

    override fun getItemCount(): Int {
        return fileList.size
    }
}