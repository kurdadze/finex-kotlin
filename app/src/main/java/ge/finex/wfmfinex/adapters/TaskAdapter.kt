package ge.finex.wfmfinex.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.models.Task
import ge.finex.wfmfinex.presentation.TaskDetailsActivity


@Suppress("CAST_NEVER_SUCCEEDS")
class TaskAdapter(private var context: Context, private val tasksList: List<Task>) :
    RecyclerView.Adapter<TaskAdapter.TaskViewFolder>() {

    inner class TaskViewFolder(v: View) : RecyclerView.ViewHolder(v) {
        var lBackground = v.findViewById<RelativeLayout>(R.id.lBackground)
        var relativeLayout = v.findViewById<RelativeLayout>(R.id.relativeLayout)
        var txtUnit = v.findViewById<TextView>(R.id.txtUnit)
        var description = v.findViewById<TextView>(R.id.description)
        var currentStatus = v.findViewById<TextView>(R.id.currentStatus)
        var relativeLayout1 = v.findViewById<RelativeLayout>(R.id.relativeLayout1)
        var txtTaskID = v.findViewById<TextView>(R.id.txtTaskID)
        var lblStart = v.findViewById<TextView>(R.id.lblStart)
        var txtDate = v.findViewById<TextView>(R.id.txtDate)
        var nonReadID = v.findViewById<ImageView>(R.id.nonReadID)
        var onHoldID = v.findViewById<ImageView>(R.id.onHoldID)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TaskViewFolder, position: Int) {
        val task = tasksList[position]

        val taskNotAccepted =
            holder.itemView.context.getString(R.string.TSK_NOT_ACCEPTED)
        val taskInProgress =
            holder.itemView.context.getString(R.string.TSK_IN_PROGRESS)
        val taskOnHold =
            holder.itemView.context.getString(R.string.TSK_ON_HOLD)
        val taskDone =
            holder.itemView.context.getString(R.string.TSK_DONE)
        val taskAccepted =
            holder.itemView.context.getString(R.string.TSK_ACCEPTED)

        val taskReadStatus: Boolean = task.isRead!!
        val currTask: Int = task.currentActionType!!

        val visibilityNonRead = if (taskReadStatus) {
            View.GONE
        } else {
            View.VISIBLE
        }

        if (currTask == 2) {
            holder.onHoldID.visibility = View.VISIBLE
        } else {
            holder.onHoldID.visibility = View.GONE
        }

        val actionColor: Int
        val actionText: String
        when (currTask.toString()) {
            "0" -> {
                actionColor = Color.RED
                actionText = taskNotAccepted
            }
            "1", "3" -> {
                actionColor = -0xff97f4
                actionText = taskInProgress
            }
            "2" -> {
                actionColor = -0xff97f4
                actionText = taskOnHold
            }
            "4", "11", "12" -> {
                actionColor = Color.GRAY
                actionText = taskDone
            }
            "7", "9", "10" -> {
                actionColor = -0xff97f4
                actionText = taskAccepted
            }
            else -> {
                actionColor = Color.BLUE
                actionText = taskNotAccepted
            }
        }

        val priorityColor: Int = when (task.priority.toString()) {
            "1" -> -0x1eb9bc //Color.RED; // წითელი
            "2" -> -0xf68ef //0xFFF19711; // ფორთოხლისფერი
            "3" -> -0x90100 //0xFFFFC400; // ყვითელი
            "4", "5" -> -0xb95c10 //Color.BLUE; // ლურჯი
            else -> -0x1eb9bc //Color.RED; // წითელი
        }

        val taskTypeDesc: String = when (task.taskType.toString()) {
            "0" -> "ERMS"
            "1" -> "OPL"
            "2" -> "INT"
            else -> "INT"
        }

        var td = task.note // Old -> description
        if (td == null) {
            td = "..."
        } else {
            if (td.length > 15) {
                td = td.substring(0, 15) + "..."
            }
        }

        val taskId: String = task.taskID.toString()

        holder.description.text = td
        holder.lBackground.setBackgroundColor(priorityColor)
        holder.nonReadID.visibility = visibilityNonRead
        holder.txtTaskID.text = "$taskTypeDesc $taskId"
        holder.txtUnit.text = task.unitName
        holder.currentStatus.setTextColor(actionColor)
        holder.currentStatus.text = actionText
        holder.currentStatus.setTextColor(actionColor)
        holder.txtDate.text = task.endDate
        holder.itemView.setOnClickListener {
            val detailsIntent = Intent(context, TaskDetailsActivity::class.java)
            detailsIntent.putExtra("currentTask", task as java.io.Serializable)
            context.startActivity(detailsIntent)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewFolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_task, parent, false)
        return TaskViewFolder(v)
    }

    override fun getItemCount(): Int {
        return tasksList.size
    }

}