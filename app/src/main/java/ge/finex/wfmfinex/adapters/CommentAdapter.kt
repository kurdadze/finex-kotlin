package ge.finex.wfmfinex.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ge.finex.wfmfinex.R
import ge.finex.wfmfinex.models.Comment

class CommentAdapter(private val commentsList: List<Comment>) :
    RecyclerView.Adapter<CommentAdapter.CommentsViewFolder>() {

    inner class CommentsViewFolder(v: View) : RecyclerView.ViewHolder(v) {
        var createdByName: TextView = v.findViewById(R.id.cmtCreatedByNameID)
        var comment: TextView = v.findViewById(R.id.cmtCommentID)
        var createDate: TextView = v.findViewById(R.id.cmtCreateDateID)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CommentsViewFolder, position: Int) {
        val comment = commentsList[position]

        holder.createdByName.text = comment.createdByName
        holder.comment.text = comment.comment
        holder.createDate.text = comment.createDate
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewFolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_comment, parent, false)
        return CommentsViewFolder(v)
    }

    override fun getItemCount(): Int {
        return commentsList.size
    }

}